/* SPDX-FileCopyrightText: 2005, 2014 - Paolo Borelli
 * SPDX-FileCopyrightText: 2010 - Jesse van den Kieboom
 * SPDX-FileCopyrightText: 2010 - Ignacio Casal Quinteiro
 * SPDX-FileCopyrightText: 2013, 2016, 2025 - Sébastien Wilmet
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <libpeas/peas-extension-base.h>
#include <libpeas/peas-object-module.h>

G_BEGIN_DECLS

#define GEDIT_TYPE_SORT_PLUGIN		(gedit_sort_plugin_get_type ())
#define GEDIT_SORT_PLUGIN(o)		(G_TYPE_CHECK_INSTANCE_CAST ((o), GEDIT_TYPE_SORT_PLUGIN, GeditSortPlugin))
#define GEDIT_SORT_PLUGIN_CLASS(k)	(G_TYPE_CHECK_CLASS_CAST((k), GEDIT_TYPE_SORT_PLUGIN, GeditSortPluginClass))
#define GEDIT_IS_SORT_PLUGIN(o)		(G_TYPE_CHECK_INSTANCE_TYPE ((o), GEDIT_TYPE_SORT_PLUGIN))
#define GEDIT_IS_SORT_PLUGIN_CLASS(k)	(G_TYPE_CHECK_CLASS_TYPE ((k), GEDIT_TYPE_SORT_PLUGIN))
#define GEDIT_SORT_PLUGIN_GET_CLASS(o)	(G_TYPE_INSTANCE_GET_CLASS ((o), GEDIT_TYPE_SORT_PLUGIN, GeditSortPluginClass))

typedef struct _GeditSortPlugin         GeditSortPlugin;
typedef struct _GeditSortPluginClass    GeditSortPluginClass;
typedef struct _GeditSortPluginPrivate  GeditSortPluginPrivate;

struct _GeditSortPlugin
{
	PeasExtensionBase parent;

	GeditSortPluginPrivate *priv;
};

struct _GeditSortPluginClass
{
	PeasExtensionBaseClass parent_class;
};

GType	gedit_sort_plugin_get_type	(void) G_GNUC_CONST;

G_MODULE_EXPORT
void	peas_register_types		(PeasObjectModule *module);

G_END_DECLS
