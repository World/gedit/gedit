/* SPDX-FileCopyrightText: 2014 - Ignacio Casal Quinteiro
 * SPDX-FileCopyrightText: 2025 - Sébastien Wilmet
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <libpeas/peas.h>

G_BEGIN_DECLS

#define GEDIT_TYPE_SPELL_APP_ACTIVATABLE             (gedit_spell_app_activatable_get_type ())
#define GEDIT_SPELL_APP_ACTIVATABLE(obj)             (G_TYPE_CHECK_INSTANCE_CAST ((obj), GEDIT_TYPE_SPELL_APP_ACTIVATABLE, GeditSpellAppActivatable))
#define GEDIT_SPELL_APP_ACTIVATABLE_CLASS(klass)     (G_TYPE_CHECK_CLASS_CAST ((klass), GEDIT_TYPE_SPELL_APP_ACTIVATABLE, GeditSpellAppActivatableClass))
#define GEDIT_IS_SPELL_APP_ACTIVATABLE(obj)          (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GEDIT_TYPE_SPELL_APP_ACTIVATABLE))
#define GEDIT_IS_SPELL_APP_ACTIVATABLE_CLASS(klass)  (G_TYPE_CHECK_CLASS_TYPE ((klass), GEDIT_TYPE_SPELL_APP_ACTIVATABLE))
#define GEDIT_SPELL_APP_ACTIVATABLE_GET_CLASS(obj)   (G_TYPE_INSTANCE_GET_CLASS ((obj), GEDIT_TYPE_SPELL_APP_ACTIVATABLE, GeditSpellAppActivatableClass))

typedef struct _GeditSpellAppActivatable         GeditSpellAppActivatable;
typedef struct _GeditSpellAppActivatableClass    GeditSpellAppActivatableClass;
typedef struct _GeditSpellAppActivatablePrivate  GeditSpellAppActivatablePrivate;

struct _GeditSpellAppActivatable
{
	GObject parent;

	GeditSpellAppActivatablePrivate *priv;
};

struct _GeditSpellAppActivatableClass
{
	GObjectClass parent_class;
};

GType	gedit_spell_app_activatable_get_type	(void);

void	gedit_spell_app_activatable_register	(PeasObjectModule *module);

G_END_DECLS
