/* SPDX-FileCopyrightText: 2002-2005 - Paolo Maggi
 * SPDX-FileCopyrightText: 2025 - Sébastien Wilmet
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <libpeas/peas.h>

G_BEGIN_DECLS

#define GEDIT_TYPE_SPELL_WINDOW_ACTIVATABLE             (gedit_spell_window_activatable_get_type ())
#define GEDIT_SPELL_WINDOW_ACTIVATABLE(obj)             (G_TYPE_CHECK_INSTANCE_CAST ((obj), GEDIT_TYPE_SPELL_WINDOW_ACTIVATABLE, GeditSpellWindowActivatable))
#define GEDIT_SPELL_WINDOW_ACTIVATABLE_CLASS(klass)     (G_TYPE_CHECK_CLASS_CAST ((klass), GEDIT_TYPE_SPELL_WINDOW_ACTIVATABLE, GeditSpellWindowActivatableClass))
#define GEDIT_IS_SPELL_WINDOW_ACTIVATABLE(obj)          (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GEDIT_TYPE_SPELL_WINDOW_ACTIVATABLE))
#define GEDIT_IS_SPELL_WINDOW_ACTIVATABLE_CLASS(klass)  (G_TYPE_CHECK_CLASS_TYPE ((klass), GEDIT_TYPE_SPELL_WINDOW_ACTIVATABLE))
#define GEDIT_SPELL_WINDOW_ACTIVATABLE_GET_CLASS(obj)   (G_TYPE_INSTANCE_GET_CLASS ((obj), GEDIT_TYPE_SPELL_WINDOW_ACTIVATABLE, GeditSpellWindowActivatableClass))

typedef struct _GeditSpellWindowActivatable         GeditSpellWindowActivatable;
typedef struct _GeditSpellWindowActivatableClass    GeditSpellWindowActivatableClass;
typedef struct _GeditSpellWindowActivatablePrivate  GeditSpellWindowActivatablePrivate;

struct _GeditSpellWindowActivatable
{
	GObject parent;

	GeditSpellWindowActivatablePrivate *priv;
};

struct _GeditSpellWindowActivatableClass
{
	GObjectClass parent_class;
};

GType	gedit_spell_window_activatable_get_type (void) G_GNUC_CONST;

void	gedit_spell_window_activatable_register (PeasObjectModule *module);

G_END_DECLS
