/*
 * This file is part of gedit
 *
 * Copyright (C) 2001-2005 Paolo Maggi
 * Copyright (C) 2024 Sébastien Wilmet
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GEDIT_PREFERENCES_DIALOG_H
#define GEDIT_PREFERENCES_DIALOG_H

#include <tepl/tepl.h>

G_BEGIN_DECLS

#define GEDIT_TYPE_PREFERENCES_DIALOG             (gedit_preferences_dialog_get_type ())
#define GEDIT_PREFERENCES_DIALOG(obj)             (G_TYPE_CHECK_INSTANCE_CAST ((obj), GEDIT_TYPE_PREFERENCES_DIALOG, GeditPreferencesDialog))
#define GEDIT_PREFERENCES_DIALOG_CLASS(klass)     (G_TYPE_CHECK_CLASS_CAST ((klass), GEDIT_TYPE_PREFERENCES_DIALOG, GeditPreferencesDialogClass))
#define GEDIT_IS_PREFERENCES_DIALOG(obj)          (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GEDIT_TYPE_PREFERENCES_DIALOG))
#define GEDIT_IS_PREFERENCES_DIALOG_CLASS(klass)  (G_TYPE_CHECK_CLASS_TYPE ((klass), GEDIT_TYPE_PREFERENCES_DIALOG))
#define GEDIT_PREFERENCES_DIALOG_GET_CLASS(obj)   (G_TYPE_INSTANCE_GET_CLASS ((obj), GEDIT_TYPE_PREFERENCES_DIALOG, GeditPreferencesDialogClass))

typedef struct _GeditPreferencesDialog         GeditPreferencesDialog;
typedef struct _GeditPreferencesDialogClass    GeditPreferencesDialogClass;
typedef struct _GeditPreferencesDialogPrivate  GeditPreferencesDialogPrivate;

struct _GeditPreferencesDialog
{
	TeplPrefsDialog parent;

	GeditPreferencesDialogPrivate *priv;
};

struct _GeditPreferencesDialogClass
{
	TeplPrefsDialogClass parent_class;
};

GType			gedit_preferences_dialog_get_type	(void);

TeplPrefsDialog *	gedit_preferences_dialog_new		(void);

G_END_DECLS

#endif /* GEDIT_PREFERENCES_DIALOG_H */
